## Formando nueva lista en python

Para este ejercicio hay que recordar sobre como Python agrega valores en una lista, asimismo el uso de la función `range` y el uso de las funciones nativas.

Define la función `listing_fruits` que recibe una lista de frutas como argumento y retorna una nueva lista. El resultado del `driver code` debe ser `True`.


```python
"""listing_fruits function"""


#driver code
print(listing_fruits(["Banana", "Strawberry", "Lemon"]) == [1, "Banana", 2, "Strawberry", 3, "Lemon"])
```

